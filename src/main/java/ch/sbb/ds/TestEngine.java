package ch.sbb.ds;
import lombok.extern.slf4j.Slf4j;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.TimeoutException;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.List;

@Slf4j
public class TestEngine implements AutoCloseable {

    private WebDriver driver = null;
    private WebDriverWait waitDriver = null;

    private static final long TIMEOUT_SECONDS = 12;
    private static final String ELEMENT_ERROR_TEXT = "Error: no such element {} {}";
    private static final String TIMEOUT_ERROR_TEXT = "Error: time out after {} seconds of element '{}' URL: {}";
    private static final String WEBDRIVER_ERROR_TEXT = "WebDriver or WaitDriver ain't initialized";

    private static ChromeOptions createChromeOptions(boolean hFlag) {
        ChromeOptions chromeOptions = new ChromeOptions();
        if(hFlag) chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--disable-gpu");
        chromeOptions.addArguments("--disable-background-networking");
        chromeOptions.addArguments("--disable-default-apps");
        chromeOptions.addArguments("--disable-extensions");
        chromeOptions.addArguments("--disable-sync");
        chromeOptions.addArguments("--disable-translate");
        chromeOptions.addArguments("--hide-scrollbars");
        chromeOptions.addArguments("--metrics-recording-only");
        chromeOptions.addArguments("--mute-audio");
        chromeOptions.addArguments("--no-first-run");
        chromeOptions.addArguments("--safebrowsing-disable-auto-update");
        chromeOptions.addArguments("--disable-sync");
        return chromeOptions;
    }

    void delay (int time) {
        try {
            Thread.sleep(time);
        } catch (Exception ex) {
            log.info("{}", ex.toString());
        }
    }

    List<WebElement> findListByCssSelector(String name){
        if(checkPreconditions()) {
            try {
                waitDriver.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(name)));
            } catch (TimeoutException et){
                log.info(TIMEOUT_ERROR_TEXT, TIMEOUT_SECONDS, name, driver.getCurrentUrl());
                return Collections.emptyList();
            }
            try {
                return driver.findElements(By.cssSelector(name));
            } catch (NoSuchElementException e) {
                log.info(TIMEOUT_ERROR_TEXT, TIMEOUT_SECONDS, name, driver.getCurrentUrl());
            }
        }
        return Collections.emptyList();
    }

    List<WebElement> findListByCssSelector(WebElement anchor, String name){
        try {
            return anchor.findElements(By.cssSelector(name));
        } catch (NoSuchElementException e) {
            log.info(ELEMENT_ERROR_TEXT, name, driver.getCurrentUrl());
        }
        return Collections.emptyList();
    }

    List<WebElement> findListByXPath(WebElement anchor, String name){
        try {
            return anchor.findElements(By.xpath(name));
        } catch (NoSuchElementException e) {
            log.info(ELEMENT_ERROR_TEXT, name, driver.getCurrentUrl());
        }
        return Collections.emptyList();
    }

    final List<WebElement> findListById(String name) {
        if(checkPreconditions()) {
            try {
                waitDriver.until(ExpectedConditions.visibilityOfElementLocated(By.id(name)));
            } catch ( TimeoutException et){
                log.info(TIMEOUT_ERROR_TEXT, TIMEOUT_SECONDS, name, driver.getCurrentUrl());
                return Collections.emptyList();
            }
            try {
                return driver.findElements(By.id(name));
            } catch (NoSuchElementException e) {
                log.info(ELEMENT_ERROR_TEXT, name, driver.getCurrentUrl());
            }
        }
        return Collections.emptyList();
    }

    @Nullable
    final WebElement findByXPath(String name) {
        if (checkPreconditions()) {
            return subPartofSearch(name, driver, waitDriver, true);
        }else{
            log.info("Web-Driver or Web-Wait-Driver is null");
        }
        return null;
    }


    @Nullable
    private WebElement subPartofSearch(String name, WebDriver selectDriver, WebDriverWait selectWaitDriver, boolean visiFlag ) {
        if(visiFlag) {
            try {
                selectWaitDriver.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(name)));
            } catch (TimeoutException et) {
                log.info(TIMEOUT_ERROR_TEXT, TIMEOUT_SECONDS, name, selectDriver.getCurrentUrl());
                return null;
            }
        }
        try {
            return selectDriver.findElement(By.xpath(name));
        } catch (NoSuchElementException e) {
            log.info(ELEMENT_ERROR_TEXT, name, selectDriver.getCurrentUrl());
        }
        return null;
    }


    private boolean checkPreconditions(){
        if((driver != null)&&(waitDriver != null)) {
            return true;
        }
        else{
           log.info(WEBDRIVER_ERROR_TEXT);
        }
        return false;
    }

    WebDriver getFrame(String title){
        if (checkPreconditions()) {
            try {
                waitDriver.until(ExpectedConditions.visibilityOfElementLocated(By.tagName(title)));
            } catch (TimeoutException et) {
                log.info(TIMEOUT_ERROR_TEXT, TIMEOUT_SECONDS, title, driver.getCurrentUrl());
                return null;
            }
            return driver.switchTo().frame(driver.findElement(By.tagName(title)));
        }
        return null;
    }

    @Nullable
    WebElement findByCssSelector(String name, boolean visibility) {
        if(checkPreconditions()) {
            if(visibility) {
                try {
                    waitDriver.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(name)));
                } catch (TimeoutException et) {
                    log.info(TIMEOUT_ERROR_TEXT, TIMEOUT_SECONDS, name, driver.getCurrentUrl());
                    return null;
                }
            }
            try {
                return driver.findElement(By.cssSelector(name));
            } catch (NoSuchElementException e) {
                log.info(ELEMENT_ERROR_TEXT, name, driver.getCurrentUrl());
            }
        }
        return null;
    }

    final void go2Url(String url, boolean refreshFlag){
        if (checkPreconditions()) {
            driver.navigate().to(url);
            if(refreshFlag) driver.navigate().refresh();
        }
    }

    final void getFocus(WebElement theElement){
        if (checkPreconditions()) {
            Actions builder = new Actions(driver);
            builder.moveToElement(theElement).perform();
        }
    }

    final void sendActionKeys(Keys keys){
        Actions builder = new Actions(driver);
        Action sendKeys = builder.sendKeys(keys).build();
        sendKeys.perform();
    }

    private void startChromeDriver(boolean hFlag){
        try {
            driver = new ChromeDriver(createChromeOptions(hFlag));
        }catch (SessionNotCreatedException ex){
            log.info("Please get the latest ChromeDriver https://sites.google.com/a/chromium.org/chromedriver/downloads");
            log.info("{}",ex.toString());
            driver = null;
        }
    }

    TestEngine(String webUrl) {

        log.info("Start Web-Engine: ");

        startChromeDriver(false);

        if (driver != null) {
           waitDriver = new WebDriverWait(driver, TIMEOUT_SECONDS);
           driver.get(webUrl);
        } else {
           log.info("Error: No further test functions can be executed as a result of a zeroed pointer.");
        }
    }

    @Override
    public void close() {
        if(driver != null){
            driver.close();
            driver.quit();
            log.info("The test driver has been stopped");
        }
    }
}
