package ch.sbb.ds;

import lombok.extern.slf4j.Slf4j;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.ResourceUtils;

import java.util.Scanner;

@Slf4j
@SpringBootApplication
public class TestApp implements CommandLineRunner{

	@Value("${file.path.chromedriver}")
	private String chromeDriverPath;

	private String menuChoice;
	
	private static final int STANDARD_DELAY = 4000;
	
	private static final String WEB_URL = "http://www.sbb.ch";

	private static final String CHROME_KEY = "webdriver.chrome.driver";
	
	private static final String SEPARATOR = "-----------------------------------------------------------------------";
		
	public void setWebDriver(String driverPath){
		try {
			log.info("Set system environment for local web drivers");
			System.setProperty(CHROME_KEY, ResourceUtils.getFile(driverPath).getAbsolutePath());
		}catch(Exception ex){
			log.info("Web driver environment IO Error: {}", ex);
		}
	}

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(TestApp.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.run(args);
	}

	private boolean searchTest(String departure, String destination, String time){
		try (TestEngine auto = new TestEngine(WEB_URL)) {
			
			// Arrange

			WebElement fromField = auto.findByCssSelector("input[id='fromField']",false);
			WebElement toField = auto.findByCssSelector("input[id='toField']",false);

			if(fromField != null){
				fromField.sendKeys(departure);
			}

			if(toField != null){
				toField.sendKeys(destination);
			}

			WebElement timeField = auto.findByCssSelector("input[id='timepicker']", false);

			if(timeField != null){
				timeField.clear();
				timeField.sendKeys(time);
				
				// Act
				
				timeField.sendKeys(Keys.ENTER);
			}

			WebElement originWeb  = auto.findByXPath("//*[@class='mod_pagetitle_origin_destination']");
			WebElement targetWeb  = auto.findByXPath("//*[@class='mod_pagetitle_target_destination']");

			if((originWeb != null)&&(targetWeb != null)) {

				String originText = originWeb.getText().replaceAll("(\\n)", " ");
				String targetText = targetWeb.getText().replaceAll("(\\n)", " ");
				
				// Assert

				if(originWeb.getText().contains(departure)&&targetWeb.getText().contains(destination)){
					log.info("{} {}", originText, targetText);
								
					return true;
				}
			
				// Take a look at the screen
			
				auto.delay(STANDARD_DELAY);
			}
		
		} catch(Exception ex) {
			log.info("Error: Test automation sequence has failed {}", ex.toString());
		}

		return false;
	}
	
	@Override
	public void run(String... args) {

		// *** Set Chrome Drivers ***

		setWebDriver(chromeDriverPath);

		// *** Start menu ***

		log.info("1) Start Tests");
		log.info("0) Exit");

		do
		{
			log.info("Choose:");
			
			Scanner keyboard = Scanner(System.in);
			menuChoice = keyboard.next();

			switch(menuChoice) {
                case "0": 	break;
				case "1":
					log.info(SEPARATOR);
					log.info("Test Basel - Lugano : {}",searchTest("Basel","Lugano","10:00")? "passed":"failed");
					log.info(SEPARATOR);
					log.info("Test Genf  - Chur   : {}",searchTest("Genf","Chur","12:00")? "passed":"failed");
					log.info(SEPARATOR);
				default:
		
			}
			
		}
		while(!menuChoice.equals("0"));
	
		System.exit(0);
	}
}